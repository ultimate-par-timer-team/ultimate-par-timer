#include <EEPROM.h>

struct StoreData {
  bool valid;
  uint8_t version;
  uint16_t serialNum;
  uint8_t greeting[4];
} store = { valid: false, version: 0, serialNum: 0xFFFF };

void initStore(uint16_t serialNum) {
  EEPROM.write(0, 'U');
  EEPROM.write(1, 'P');
  EEPROM.write(2, 'T');
  EEPROM.write(3, 0);
  EEPROM.write(4, SYM_EMPTY);
  EEPROM.write(5, SYM_H);
  EEPROM.write(6, SYM_I);
  EEPROM.write(7, SYM_EMPTY);

  EEPROM.write(8, serialNum & 0xFF);
  EEPROM.write(9, serialNum >> 8);
}

void readStore() {
  if (EEPROM.read(0) != 'U' ||
      EEPROM.read(1) != 'P' ||
      EEPROM.read(2) != 'T') 
  {
    store.valid = false;
  }
  store.valid = true;
  store.version = EEPROM.read(3);
  store.greeting[0] = EEPROM.read(4);
  store.greeting[1] = EEPROM.read(5);
  store.greeting[2] = EEPROM.read(6);
  store.greeting[3] = EEPROM.read(7);
  store.serialNum = EEPROM.read(8) | (EEPROM.read(9) << 8);
}
