#include "encoder.h"

enum KeyState {
  KEY_UP,
  KEY_DOWN,
  KEY_BOUNCING,
};

struct KeyControlData {
  int pin;
  bool enableAutoRepeat;
  KeyState state;
  long t;
};

struct KeyControlData startKeyData = { pin: 4, enableAutoRepeat: false, state: KEY_UP };
struct KeyControlData menuKeyData = { pin: 5, enableAutoRepeat: false, state: KEY_UP };
struct KeyControlData minusKeyData = { pin: 6, enableAutoRepeat: true, state: KEY_UP };
struct KeyControlData plusKeyData = { pin: 7, enableAutoRepeat: true, state: KEY_UP };

bool startKeyDown = false;
bool menuKeyDown = false;
bool minusKeyDown = false;
bool plusKeyDown = false;

enum KeyFlags {
  NO_KEYS = 0,
  START_KEY = 1,
  MENU_KEY = 2,
  MINUS_KEY = 4,
  PLUS_KEY = 8
};

unsigned lastKeypadState = NO_KEYS;

const long debounceInterval = 50;
const long autoRepeatTimeout = 1000;
const long autoRepeatInterval = 100;

uint16_t currentEncoderCounter;

void setupKeypad() {
  pinMode(startKeyData.pin, INPUT_PULLUP);
  pinMode(menuKeyData.pin, INPUT_PULLUP);
  pinMode(minusKeyData.pin, INPUT_PULLUP);
  pinMode(plusKeyData.pin, INPUT_PULLUP);

  setupEncoder();
  currentEncoderCounter = encoder.getCounter();
}

bool handleKeyRepeat(struct KeyControlData *data) {
  if (!data->enableAutoRepeat) return true;
  long passed = millis() - data->t;
  if (passed < autoRepeatTimeout) return true;
  long half_intervals = (autoRepeatTimeout - passed) * 2 / autoRepeatInterval;
  return (half_intervals & 1);
}

bool keyRead(struct KeyControlData *data) {
  bool down = !digitalRead(data->pin);
  if (down) {
    if (data->state != KEY_DOWN) {
      data->t = millis();
      data->state = KEY_DOWN;
    }
    return handleKeyRepeat(data);
  }
  switch (data->state) {
    case KEY_UP:
      return false;
    case KEY_DOWN:
      data->t = millis() + debounceInterval;
      data->state = KEY_BOUNCING;
      return handleKeyRepeat(data);
    case KEY_BOUNCING:
      if (millis() < data->t) return true;
      data->state = KEY_UP;
      return false;
  }
}

void handleEncoder(bool *minusKeyDown, bool *plusKeyDown) {
  int16_t delta = (int16_t)(encoder.getCounter() - currentEncoderCounter);
  if (delta > 0) {
    currentEncoderCounter++;
    *plusKeyDown = true;
  } else if (delta < 0) {
    currentEncoderCounter--;    
    *minusKeyDown = true;
  }
}

unsigned handleKeypad() {
  startKeyDown = keyRead(&startKeyData);
  menuKeyDown = keyRead(&menuKeyData);
  minusKeyDown = keyRead(&minusKeyData);
  plusKeyDown = keyRead(&plusKeyData);

  handleEncoder(&minusKeyDown, &plusKeyDown);
  
  unsigned state = 
    (startKeyDown ? START_KEY : 0) |
    (menuKeyDown ? MENU_KEY : 0) |
    (minusKeyDown ? MINUS_KEY : 0) |
    (plusKeyDown ? PLUS_KEY : 0);

  unsigned pressed = state & ~lastKeypadState;
  lastKeypadState = state;
  return pressed;
}
