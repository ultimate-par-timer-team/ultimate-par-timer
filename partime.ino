
#include "standby_pcm.h"
#include "pcm_player.h"
#include "display.h"
#include "keypad.h"
#include "store.h"

int ledPin = 13;

enum AppScreen {
  INVALID,
  PAR_TIME_SCREEN,
  STANDBY_SETUP_SCREEN,
  WAIT_FOR_BEEP_SCREEN,
  STANDBY_WAIT_SCREEN,
  PAUSE_WAIT_SCREEN,
  RUNNING_SCREEN,
  MIN_DELAY_SETUP_SCREEN,
  MAX_DELAY_SETUP_SCREEN,
};

const int AUTO_SET_PAR_TIME = -1;

struct AppState {
  int parTime; // deciseconds, -1 - auto-set
  int standbyTime; // deciseconds, 0 - disabled
  int minDelay; // deciseconds, 0 - disabled
  int maxDelay; // deciseconds, disabled if minDelay = 0
  enum AppScreen currentScreen;
  unsigned currentlyPressedKeys;
  union {
    struct {
      unsigned long started;
      unsigned long end;
    } interval;
  };
} appState;

void playTone() {
    tone(speakerPin, 1500, 200);
}

bool startCycle() {
  appState.interval.started = millis();
  long delay = random(appState.minDelay * 100, appState.maxDelay * 100);
  appState.interval.end = appState.interval.started + delay;
  appState.currentScreen = PAUSE_WAIT_SCREEN;
  return true;
}

bool resetCycle() {
  noTone(speakerPin);
  return startCycle();
}

bool abortCycle() {
  appState.currentScreen = PAR_TIME_SCREEN;
  stopPlayback();
  noTone(speakerPin);
  return true;
}

bool endCycle() {
  if (appState.standbyTime > 0) {
    appState.currentScreen = STANDBY_WAIT_SCREEN;
    appState.interval.started = millis();
    appState.interval.end = appState.interval.started + appState.standbyTime * 100;
    return true;
  }
  appState.currentScreen = PAR_TIME_SCREEN;
  return true;
}

bool handleKeysDuringRun() {
  if (appState.currentlyPressedKeys & START_KEY) {
    if (appState.parTime == AUTO_SET_PAR_TIME) {
      appState.parTime = (millis() - appState.interval.started) / 100;
      appState.currentScreen = PAR_TIME_SCREEN;
      return true;
    }
    return resetCycle();
  }      
  if (appState.currentlyPressedKeys & (MENU_KEY | PLUS_KEY | MINUS_KEY)) {
    return abortCycle();
  }
  return false;
}

void fixHighDelay() {
  if (appState.minDelay > appState.maxDelay) {
    appState.maxDelay = appState.minDelay;
  }
  if (appState.minDelay == 0) {
    appState.maxDelay = 0;
  }
}

bool updateState() {
  switch (appState.currentScreen) {
    case PAR_TIME_SCREEN:
      if (appState.currentlyPressedKeys & MENU_KEY) {
        appState.currentScreen = STANDBY_SETUP_SCREEN;
        return true;
      }
      if (appState.currentlyPressedKeys & MINUS_KEY) {
        if (appState.parTime > -1) appState.parTime--;
        return true;
      }
      if (appState.currentlyPressedKeys & PLUS_KEY) {
        if (appState.parTime < 999) appState.parTime++;
        return true;
      }
      if (appState.currentlyPressedKeys & START_KEY) {
        return startCycle();
      }
      break;
    case STANDBY_SETUP_SCREEN:
      if (appState.currentlyPressedKeys & MENU_KEY) {
        appState.currentScreen = MIN_DELAY_SETUP_SCREEN;
        return true;
      }
      if (appState.currentlyPressedKeys & MINUS_KEY) {
        if (appState.standbyTime > 0) appState.standbyTime--;
        return true;
      }
      if (appState.currentlyPressedKeys & PLUS_KEY) {
        if (appState.standbyTime < 999) appState.standbyTime++;
        return true;
      }
      if (appState.currentlyPressedKeys & START_KEY) {
        appState.currentScreen = PAR_TIME_SCREEN;
        return true;
      }
      break;
    case MIN_DELAY_SETUP_SCREEN:
      if (appState.currentlyPressedKeys & MENU_KEY) {
        fixHighDelay();
        appState.currentScreen = appState.minDelay != 0 ?
          MAX_DELAY_SETUP_SCREEN : PAR_TIME_SCREEN;
        return true;
      }
      if (appState.currentlyPressedKeys & MINUS_KEY) {
        if (appState.minDelay > 0) appState.minDelay--;
        return true;
      }
      if (appState.currentlyPressedKeys & PLUS_KEY) {
        if (appState.minDelay < 999) appState.minDelay++;
        return true;
      }
      if (appState.currentlyPressedKeys & START_KEY) {
        fixHighDelay();
        appState.currentScreen = PAR_TIME_SCREEN;
        return true;
      }
      break;
    case MAX_DELAY_SETUP_SCREEN:
      if (appState.currentlyPressedKeys & MENU_KEY) {
        appState.currentScreen = PAR_TIME_SCREEN;
        return true;
      }
      if (appState.currentlyPressedKeys & MINUS_KEY) {
        if (appState.maxDelay > appState.minDelay) appState.maxDelay--;
        return true;
      }
      if (appState.currentlyPressedKeys & PLUS_KEY) {
        if (appState.maxDelay < 999) appState.maxDelay++;
        return true;
      }
      if (appState.currentlyPressedKeys & START_KEY) {
        appState.currentScreen = PAR_TIME_SCREEN;
        return true;
      }
      break;
    case STANDBY_WAIT_SCREEN:
      if (handleKeysDuringRun()) {
        return true;
      }      
      if (appState.interval.end <= millis()) {
        startPlayback();
        return startCycle();
      }
      return true;
    case PAUSE_WAIT_SCREEN:
      if (handleKeysDuringRun()) {
        return true;
      }      
      if (appState.interval.end <= millis()) {
        int parTime = appState.parTime;
        if (parTime == AUTO_SET_PAR_TIME) {
          parTime = 999;
        }
        playTone();
        appState.interval.started = millis();
        appState.interval.end = appState.interval.started + 100l * parTime;
        appState.currentScreen = RUNNING_SCREEN;
        return true;
      }
      return false;
    case RUNNING_SCREEN:
      if (handleKeysDuringRun()) {
        return true;
      }      
      if (appState.interval.end <= millis()) {
        playTone();
        return endCycle();
      }
      return true;
  }
  return false;
}

void encodeDisplayDecisecs(int n, bool dots) {
  int d1 = n / 100;
  int d2 = (n / 10) % 10;
  int d3 = n % 10;
  displayData[0] = d1 > 0 ? encodeDigit(d1) : SYM_EMPTY;
  displayData[1] = encodeDigit(d2);
  if (dots) {
    displayData[1] |= SYM_DOTS;
  }
  displayData[2] = encodeDigit(d3);
}

void updateScreen() {
  long interval;
  switch (appState.currentScreen) {
    case PAR_TIME_SCREEN:
      if (appState.parTime == AUTO_SET_PAR_TIME) {
        displayData[0] = SYM_S;
        displayData[1] = SYM_E;
        displayData[2] = SYM_T;
        displayData[3] = SYM_EMPTY;    
      } else {
        encodeDisplayDecisecs(appState.parTime, false);
        displayData[3] = appState.standbyTime > 0 ? SYM_UNDER : SYM_EMPTY;
      }
      updateDisplay();
      digitalWrite(ledPin, LOW);
      break;
    case STANDBY_SETUP_SCREEN:
      if (appState.standbyTime > 0) {
        encodeDisplayDecisecs(appState.standbyTime, false);
      } else {
        displayData[0] = SYM_EMPTY;
        displayData[1] = SYM_MINUS;
        displayData[2] = SYM_MINUS;     
      }
      displayData[3] = SYM_LOW_U;
      updateDisplay();
      digitalWrite(ledPin, HIGH);
      break;
    case STANDBY_WAIT_SCREEN:
      interval = appState.interval.end - millis();
      encodeDisplayDecisecs(interval / 100, true);
      displayData[3] = SYM_MINUS;
      updateDisplay();
      digitalWrite(ledPin, LOW);
      break;
    case PAUSE_WAIT_SCREEN:
      displayData[0] = displayData[1] =
      displayData[2] = displayData[3] = SYM_LOW_O;
      updateDisplay();
      digitalWrite(ledPin, LOW);
      break;
    case RUNNING_SCREEN:
      interval = millis() - appState.interval.started;
      encodeDisplayDecisecs(interval / 100, true);
      displayData[3] = SYM_EMPTY;
      updateDisplay();
      digitalWrite(ledPin, HIGH);
      break;
    case MIN_DELAY_SETUP_SCREEN:
      if (appState.minDelay > 0) {
        encodeDisplayDecisecs(appState.minDelay, false);
      } else {
        displayData[0] = SYM_EMPTY;
        displayData[1] = SYM_MINUS;
        displayData[2] = SYM_MINUS;     
      }
      displayData[3] = SYM_LOW_L;
      updateDisplay();
      digitalWrite(ledPin, HIGH);
      break;
    case MAX_DELAY_SETUP_SCREEN:
      encodeDisplayDecisecs(appState.maxDelay, false);
      displayData[3] = SYM_LOW_INV_L;
      updateDisplay();
      digitalWrite(ledPin, HIGH);
      break;
    default:
      displayData[0] = SYM_E;
      displayData[1] = SYM_R;
      displayData[2] = SYM_R;
      displayData[3] = encodeDigit(appState.currentScreen);
      updateDisplay();
      digitalWrite(ledPin, HIGH);
      break;
  }
}

void setup() {
  appState = { 20, 0, 10, 40, PAR_TIME_SCREEN };

  randomSeed(analogRead(0));

  pinMode(ledPin, OUTPUT);

  readStore();

  setupKeypad();
  setupSpeaker();
  setupDisplay(store.valid ? store.greeting : nullptr);

  updateScreen();
}

void loop() {
  appState.currentlyPressedKeys = handleKeypad();
  if (updateState()) {
    updateScreen();
  }
}
