const int encoderPinA = 2;
const int encoderPinB = 2;

class Encoder {
  private:
    int a;
    int b;
    volatile uint16_t counter;
    int lastVal;

    int getVal() const {
      return (0 - digitalRead(a)) ^ digitalRead(b);
    }

    static Encoder *currentEncoder;
    
    static void processEncoderPins() {
      currentEncoder->onEvent();
    }

    void onEvent() {
        int val = getVal();
        switch ((val - lastVal) & 3) {
          case 1: counter++; break;
          case 3: counter--; break;
        }
        lastVal = val;
    }

    
  public:
    Encoder(int pinA, int pinB) : a(pinA), b(pinB), counter(0) {
      lastVal = getVal();
    }

    uint16_t getCounter() const {
      return counter;
    }

    void begin() {
      currentEncoder = this;
      pinMode(a, INPUT_PULLUP);
      pinMode(b, INPUT_PULLUP);
      attachInterrupt(digitalPinToInterrupt(a), processEncoderPins, CHANGE);
      attachInterrupt(digitalPinToInterrupt(b), processEncoderPins, CHANGE);      
    }
} encoder(2, 3);

Encoder* Encoder::currentEncoder = nullptr;

void setupEncoder() {
  encoder.begin();
}
