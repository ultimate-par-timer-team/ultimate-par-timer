#include <TM1637Display.h>
#define CLK 9
#define DIO 10

TM1637Display tm1637_display(CLK, DIO);

//
//      A
//     ---
//  F |   | B
//     -G-
//  E |   | C
//     ---
//      D

const uint8_t SYM_EMPTY = 0;
const uint8_t SYM_DOTS = 0x80;
const uint8_t SYM_LOW_R = SEG_E | SEG_G;
const uint8_t SYM_LOW_L = SEG_E | SEG_D;
const uint8_t SYM_LOW_INV_L = SEG_C | SEG_D;
const uint8_t SYM_UNDER = SEG_D;
const uint8_t SYM_OVER = SEG_A;
const uint8_t SYM_MINUS = SEG_G;
const uint8_t SYM_LOW_U = SEG_E | SEG_C | SEG_D;
const uint8_t SYM_LOW_O = SEG_E | SEG_C | SEG_D | SEG_G;

const uint8_t SYM_H = SEG_B | SEG_C | SEG_E | SEG_F | SEG_G;
const uint8_t SYM_I = SEG_F | SEG_E;
const uint8_t SYM_R = SYM_LOW_R;
const uint8_t SYM_E = SEG_A | SEG_D | SEG_E | SEG_F | SEG_G;
const uint8_t SYM_S = SEG_A | SEG_D | SEG_C | SEG_F | SEG_G;
const uint8_t SYM_T = SEG_D | SEG_E | SEG_F | SEG_G;

uint8_t displayData[4] = { 0x00, SYM_H, SYM_I, 0x00 };
uint8_t lastDisplayData[4];

void setupDisplay(uint8_t *greeting) {
  tm1637_display.setBrightness(1);

  // Test display
  tm1637_display.setSegments(greeting ? greeting : displayData);
  delay(500);
  tm1637_display.clear();

  displayData[0] = displayData[1] =
  displayData[2] = displayData[3] = 0;
  memcpy(lastDisplayData, displayData, sizeof displayData);
}

void updateDisplay() {
  if (memcmp(lastDisplayData, displayData, sizeof displayData) == 0) {
    return;
  }
  tm1637_display.setSegments(displayData);
  memcpy(lastDisplayData, displayData, sizeof displayData);
}

uint8_t encodeDigit(int d) {
  return tm1637_display.encodeDigit(d);
}
